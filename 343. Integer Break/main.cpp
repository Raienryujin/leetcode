#include <iostream>
#include <vector>

using namespace std;

int integerBreak(int n) {
    if (n <= 3) {
        return n - 1;
    }

    vector<int> dp(n + 1, 0);
    dp[2] = 2;
    dp[3] = 3;

    for (int i = 4; i <= n; i++) {
        dp[i] = max(2 * dp[i - 2], 3 * dp[i - 3]);
    }

    return dp[n];
}

int main() {
    int n;
    cout << "Enter an integer: ";
    cin >> n;
    
    int result = integerBreak(n);
    cout << "Maximum product after breaking the integer: " << result << endl;

    return 0;
}
